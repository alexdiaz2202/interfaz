/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;
/**
 *
 * @author alexd
 * @param <E>
 */
public class Ingrediente<E>{
    String nombreIngrediente;//nombre del ingrediente
    String fechaDeCaducidad;//fecha de caducidad en formato dd/mm/aa
    
    /**
     * Constructor que crea un ingrediente en base a su nombre y fecha de caducidad
     * @param nombreI
     
     */
    public Ingrediente(String nombreI) {
      
        this.nombreIngrediente = nombreI;
    }

    // Devuelve fecha de caducidad
    public String getFechaCaducidad() {
        return fechaDeCaducidad;
    }

    //Devuelve nombre del ingrediente
    public String getNombreIngrediente() {
        return nombreIngrediente;
    }

    //Cambia el nombre del ingrediente
    public void setNombrePlatillo(String nombreI) {
        this.nombreIngrediente = nombreI;
    }

    @Override
    public String toString() {
        return "Ingrediente: "+ nombreIngrediente
               ;
    }
      
}
