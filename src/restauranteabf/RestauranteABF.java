/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;

import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author Hp beats
 */
public class RestauranteABF {

    public static void area(){
        System.out.println("1.Realizar ordenes de mesas"
        +"\n2.Revisar o realizar cambios de inventarios"
        + "\n3.Ver estado de finanzas de la caja"
        +"\n4.Salir");
    }
    public static void mesas(){
        System.out.println("1.Ordenar" 
        +"\n2. Cobrar cuenta"
        +"\n3.Cancelar cuenta");
    }
    public static void inventariosCambios(){
        System.out.println("1. Registrar compra inventario"
        +"\n2.Crear nuevo inventario"
        +"\n3.Ver Inventario");
    }
    public static void caja(){
        System.out.println("1.Registrar gasto"
        +"\n2.Registrar ingreso"
        +"\n3.Ver balance de la caja");
    }
    
    public static void main(String[] args) throws SQLException         
        {int a,b;
        CajaRest caja = new CajaRest();
        Spot mesa1 = new Spot(1);
        Spot mesa2 = new Spot(2);
        Spot mesa3 = new Spot(3);
        Spot mesa4 = new Spot(4);
        Spot mesa5 = new Spot(5);
        Spot mesa6 = new Spot(6);
        Spot mesa7 = new Spot(7);
        Spot mesa8 = new Spot(8);
        Spot mesa9 = new Spot(9);
        Spot mesa10 = new Spot(10);
        Spot[] mesas = {mesa1,mesa2,mesa3,mesa4,mesa5,mesa6,mesa7,mesa8,
            mesa9,mesa10};
        Menu menu = new Menu();
        Inventario papas = new Inventario("papas");
        Inventario carne = new Inventario("carne");
        Inventario pan = new Inventario("pan");
        Inventario lechuga = new Inventario("lechuga");
        Inventario jitomate = new Inventario("jitomate");
        Inventario agua = new Inventario("agua");
        Inventario queso = new Inventario("queso");
        Inventario[] inventarito={papas,carne,pan,lechuga,jitomate,agua,queso};
        InventarioGeneral inv = new InventarioGeneral(inventarito);
        Scanner lector = new Scanner(System.in);
        do{
        System.out.println("¿En qué área desea trabajar?");
        area();
        a = lector.nextInt();
        switch(a){
            case 1:
                System.out.println("¿Qué acción desea realizar?");
                mesas();
                b = lector.nextInt();
                switch(b){
                    case 1:
                        System.out.println("¿Que platillo desdea ordenar?");
                        System.out.println(menu.toString());
                        int c = lector.nextInt();
                        System.out.println("¿Para qué mesa?");
                        b = lector.nextInt();
                        for (int i = 0; i < mesas.length; i++) {
                            if(mesas[i].getnMesa()==b){
                                mesas[i].agregarCuenta(c, inv);
                                System.out.println(mesas[i].toString());
                            }
                        }
                    break;
                        case 2:
                        System.out.println("¿De qué mesa?");
                         b = lector.nextInt();
                        for (int i = 0; i < mesas.length; i++) {
                            if(mesas[i].getnMesa()==b){
                                mesas[i].pagarCuenta(caja);
                                System.out.println(mesas[i].toString());
                            }
                        }
                    break;
                    case 3:
                        System.out.println("¿De qué mesa?");
                        b = lector.nextInt();
                        for (int i = 0; i < mesas.length; i++) {
                            if(mesas[i].getnMesa()==b){
                                mesas[i].pagarCuenta(caja);
                                System.out.println(mesas[i].toString());
                        }
                    }
                    break;
                }
            break;
            case 2:
                System.out.println("¿Qué acción desea realizar?");
                inventariosCambios();
                b = lector.nextInt();
                switch(b){
                case 1:
                    System.out.println("Que ingrediente?");
                    String s = lector.next().toLowerCase();
                    System.out.println("Escribe el costo unitario del ingrediente");
                    double costo = lector.nextDouble();
                    System.out.println("Escriba la cantidad comprada en la unidad base");
                    int quantity = lector.nextInt();
                    inv.comprarInventarioExistente(s,quantity, costo, caja);
                    System.out.println(inv.toString());
                    InventarioDAO dao2 = new InventarioDAO();
                    for (int i = 0; i < quantity; i++) {
                        Ingrediente nuevo = new Ingrediente(s);
                        if(s == "carne"){
                        dao2.insertarCarne(nuevo);
                        }else if(s.equals("pan")){
                        dao2.insertarPan(nuevo);
                        }else if(s.equals("jitomate")){
                        dao2.insertarJitomate(nuevo);
                        }else if(s.equals("queso")){
                        dao2.insertarQueso(nuevo);
                        }else if(s.equals("lechuga")){
                        dao2.insertarLechuga(nuevo);
                        }else if(s.equals("cebolla")){
                        dao2.insertarCebolla(nuevo);
                        }else if(s.equals("papas")){
                        dao2.insertarPapas(nuevo);
                        }else{
                        dao2.insertarAgua(nuevo);
                        }
                    }
                break;
                case 2:
                    System.out.println("¿De que quiere que sea el nuevo inventario?");
                    String nuevo = lector.next().toLowerCase();
                    inv.crearInventarioNuevo(nuevo);
                    System.out.println(inv.toString());
                break;
                case 3:
                    System.out.println(inv.toString());
                break;
                
                }
            break;
            case 3:
                System.out.println("¿Qué acción desea realizar?");
                caja();
                b = lector.nextInt();
                switch(b){
                    case 1: 
                        System.out.println("Ingrese la cantidad del gasto");
                        double gasto = lector.nextDouble();
                        caja.comprar(gasto);
                        System.out.println(caja.toString());
                    break;
                    case 2:
                        System.out.println("Ingrese la cantidad del ingreso");
                        double ingreso = lector.nextDouble();
                        caja.vender(ingreso);
                        System.out.println(caja.toString());
                    break;
                    case 3:
                        System.out.println(caja.toString());
                    break;
                }
        }
        }while(a!=4);
        RestauranteDAO dao = new RestauranteDAO();
        dao.insertar(caja);
        dao.listar().stream().forEach(System.out::println);
        
    }
}

