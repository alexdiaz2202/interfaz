package restauranteabf;


import java.sql.Connection; 
import java.sql.DriverManager;
import java.sql.ResultSet; 
import java.sql.SQLException; 
import java.sql.Statement; 
import java.util.ArrayList;
import java.util.List;


public class RestauranteDAO implements DAOGenerico<CajaRest, Double> {
    static final String DATABASE_URL="jdbc:sqlite:restaurante.db";
    private Connection conexion;
    
    //Metodo constructor
    public RestauranteDAO() throws SQLException{
        conexion=DriverManager.getConnection(DATABASE_URL);
    }

    //Metodo que insertaun elemento a la tabla
    @Override
    public void insertar(CajaRest cajita) throws SQLException {
        Statement stmt=conexion.createStatement();
        stmt.executeUpdate("insert into Caja values ('"+cajita.getVentasDia()+"', '"+cajita.getComprasDia()+"')");
        stmt.close();
    }
    
    //Metodo que elimina un elemento de la tabla
    @Override
    public void eliminar(CajaRest cajita) throws SQLException{
        Statement stmt=conexion.createStatement();
        stmt.executeUpdate("delete from Caja where ventasDia= '"+cajita.getVentasDia()+"'");
        stmt.close();
    }
    
    //Metodo que nos muestra un elemento de la tabla
    @Override
    public CajaRest get(Double ventaDia) throws SQLException {
        Statement stmt = conexion.createStatement();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from Caja"
                      + " where ventaDia = "
                      + "'" + ventaDia + "';"
                );
                
        rs.next();
        CajaRest cajita = new CajaRest(rs.getDouble(1), 
                                   rs.getDouble(2));
        
        rs.close();
        stmt.close();
        
        return cajita;
                
    }
    
    //Metodo que nos muestra toda la tabla
    public List<CajaRest> listar() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<CajaRest> cajas = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from Caja"
                );
        
        while(rs.next()) {
            CajaRest cajita = new CajaRest(rs.getDouble(1), 
                                   rs.getDouble(2));
            
            cajas.add(cajita);
        }
        
        rs.close();
        stmt.close();
        
        return cajas;
    }

    //Metodo que actualiza un elemento de la tabla
    @Override
    public void actualizar(CajaRest caja) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    //Metodo que cierra la conexion
    public void cerrarConexion() throws SQLException {
        conexion.close();
    }
    
      public static void main(String[] args) throws SQLException {
        RestauranteDAO dao = new RestauranteDAO();
        
//       // System.out.println(dao.get("tomate"));
//        for (Ingrediente ingrediente: dao.listar()) {
//            System.out.println(ingrediente);
//        }
        CajaRest cajita = new CajaRest(4000.0, 2000.0);
//        dao.insertar(cajita);
//        System.out.println(dao.get("lechuga"));
        
        dao.listar().stream().forEach(System.out::println);
        
        dao.cerrarConexion();
    }
    
    
    
    
}
