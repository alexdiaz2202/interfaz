/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ferxd
 */
public class InventarioDAO implements DAOGenerico<Ingrediente, Integer>{
    static final String DATABASE_URL="jdbc:sqlite:restaurante.db";
    private final Connection conexion;


    
    //Metodo constructor
    public InventarioDAO() throws SQLException{
        conexion=DriverManager.getConnection(DATABASE_URL);
        
       
    }
    
    //Metodo que inserta un tipo de inventario en la tabla
    public void insertarCarne(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        int i = getUltimoCarne();
        stmt.executeUpdate("insert into InventarioCarne values ("+ i + ",'"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    public void insertarPan(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        int i = getUltimoPan();
        stmt.executeUpdate("insert into InventarioPan values ("+ i + ",'"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    public void insertarJitomate(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        int i = getUltimoJitomate();
        stmt.executeUpdate("insert into InventarioJitomate values ("+ i + ",'"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    public void insertarQueso(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        int i = getUltimoQueso();
        stmt.executeUpdate("insert into InventarioQueso values ("+ i + ",'"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    public void insertarLechuga(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        int i = getUltimoLechuga();
        stmt.executeUpdate("insert into InventarioLechuga values ("+ i + ",'"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    public void insertarCebolla(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        int i = getUltimoCebolla();
        stmt.executeUpdate("insert into InventarioCebolla values ("+ i + ",'"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    public void insertarPapas(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        int i = getUltimoPapas();
        stmt.executeUpdate("insert into InventarioPapas values ("+ i + ",'"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    public void insertarAgua(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        int i = getUltimoAgua();
        
        stmt.executeUpdate("insert into InventarioAgua values ("+ i + ",'"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    //Metodo que elimina un tipo de inventario de la tabla
    
    public void eliminarCarne(Ingrediente ingrediente) throws SQLException{
        Statement stmt=conexion.createStatement();
        int i = getUltimoCarne()-1;
        stmt.executeUpdate("delete from InventarioCarne where numIngrediente= "+i);
        stmt.close();
    }
    
    
    public void eliminarPan(Ingrediente ingrediente) throws SQLException{
        Statement stmt=conexion.createStatement();
        int i = getUltimoPan()-1;
        stmt.executeUpdate("delete from InventarioPan where numIngrediente= "+i);
        stmt.close();
    }
    
    
    public void eliminarJitomate(Ingrediente ingrediente) throws SQLException{
        Statement stmt=conexion.createStatement();
        int i = getUltimoJitomate()-1;
        stmt.executeUpdate("delete from InventarioJitomate where numIngrediente= "+i);
        stmt.close();
    }
    
    
    public void eliminarQueso(Ingrediente ingrediente) throws SQLException{
        Statement stmt=conexion.createStatement();
        int i = getUltimoQueso()-1;
        stmt.executeUpdate("delete from InventarioQueso where numIngrediente= "+i);
        stmt.close();
    }
    
    
    public void eliminarLechuga(Ingrediente ingrediente) throws SQLException{
        Statement stmt=conexion.createStatement();
        int i = getUltimoLechuga()-1;
        stmt.executeUpdate("delete from InventarioLechuga where numIngrediente= "+i);
        stmt.close();
    }
    
    
    public void eliminarCebolla(Ingrediente ingrediente) throws SQLException{
        Statement stmt=conexion.createStatement();
        int i = getUltimoCebolla()-1;
        stmt.executeUpdate("delete from InventarioCebolla where numIngrediente= "+i);
        stmt.close();
    }
    
    public void eliminarPapas(Ingrediente ingrediente) throws SQLException{
        Statement stmt=conexion.createStatement();
        int i = getUltimoPapas()-1;
        stmt.executeUpdate("delete from InventarioPapas where numIngrediente= "+i);
        stmt.close();
    }
    
  
    public void eliminarAgua(Ingrediente ingrediente) throws SQLException{
        Statement stmt=conexion.createStatement();
        int i = getUltimoAgua()-1;
        stmt.executeUpdate("delete from InventarioAgua where numIngrediente= "+i);
        stmt.close();
    }
//    
//    //Metodo que nos muestra los inventarios de un solo tipo
//    @Override
//    public Inventario get(String inventarioD) throws SQLException {
//        Statement stmt = conexion.createStatement();
//        
//        ResultSet rs = 
//                stmt.executeQuery(
//                        "select *"
//                      + " from Inventario"
//                      + " where inventarioDe = "
//                      + "'" + inventarioD + "';"
//                );
//                
//        rs.next();
//        Inventario inventario = new Inventario(rs.getString(1));
//        
//        rs.close();
//        stmt.close();
//        
//        return inventario;
//                
//    }
//    
//    //Metodo que nos muestra toda la tabla Inventarios
   
     public List<Ingrediente> listarCarne() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from InventarioCarne"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(2));
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }
     
      public List<Ingrediente> listarPan() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from InventarioPan"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(2));
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }
      
       public List<Ingrediente> listarJitomate() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from InventarioJitomate"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(2));
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }

        public List<Ingrediente> listarQueso() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from InventarioQueso"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(2));
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }
        public List<Ingrediente> listarLechuga() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from InventarioLechuga"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(2));
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }
    
         public List<Ingrediente> listarCebolla() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from InventarioCebolla"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(2));
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }
         
        public List<Ingrediente> listarPapas() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from InventarioPapas"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(2));
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }
        
         public List<Ingrediente> listarAgua() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from InventarioAgua"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(2));
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }

    //Metodo que actualiza la tabla
    @Override
    public void actualizar(Ingrediente elemento) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
//
    //Metodo que cierra la conexion
    public void cerrarConexion() throws SQLException {
        conexion.close();
    }

      public static void main(String[] args) throws SQLException {
        InventarioDAO dao = new InventarioDAO();
        
//       // System.out.println(dao.get("tomate"));
//        for (Ingrediente ingrediente: dao.listar()) {
//            System.out.println(ingrediente);
//        }
        Ingrediente ingrediente = new Ingrediente("agua");
////        dao.actualizar(ingrediente);
        dao.insertarAgua(ingrediente);
//          System.out.println(dao.getUltimo());
        
//        System.out.println(dao.get("lechuga"));
        
        dao.listarAgua().stream().forEach(System.out::println);
        
        dao.cerrarConexion();
    }

    @Override
    public void insertar(Ingrediente elemento) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Ingrediente get(Integer llave) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void actualizar(String nombreIngreiente) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
        
    }
    
    public int getUltimoCarne() throws SQLException {
        Statement stmt = conexion.createStatement();
        int i=0;
        ResultSet rs = stmt.executeQuery(
                        "select *"+ " from InventarioCarne");
      while(rs.next()) {
          i++;  
        }
        rs.close();
        stmt.close();   
        return i;
    }
    
    public int getUltimoPan() throws SQLException {
        Statement stmt = conexion.createStatement();
        int i=0;
        ResultSet rs = stmt.executeQuery(
                        "select *"+ " from InventarioPan");
      while(rs.next()) {
          i++;  
        }
        rs.close();
        stmt.close();   
        return i;
    }
    
    public int getUltimoJitomate() throws SQLException {
        Statement stmt = conexion.createStatement();
        int i=0;
        ResultSet rs = stmt.executeQuery(
                        "select *"+ " from InventarioJitomate");
      while(rs.next()) {
          i++;  
        }
        rs.close();
        stmt.close();   
        return i;
    }
    
    public int getUltimoQueso() throws SQLException {
        Statement stmt = conexion.createStatement();
        int i=0;
        ResultSet rs = stmt.executeQuery(
                        "select *"+ " from InventarioQueso");
      while(rs.next()) {
          i++;  
        }
        rs.close();
        stmt.close();   
        return i;
    }
    
    public int getUltimoLechuga() throws SQLException {
        Statement stmt = conexion.createStatement();
        int i=0;
        ResultSet rs = stmt.executeQuery(
                        "select *"+ " from InventarioLechuga");
      while(rs.next()) {
          i++;  
        }
        rs.close();
        stmt.close();   
        return i;
    }
    
    public int getUltimoCebolla() throws SQLException {
        Statement stmt = conexion.createStatement();
        int i=0;
        ResultSet rs = stmt.executeQuery(
                        "select *"+ " from InventarioCebolla");
      while(rs.next()) {
          i++;  
        }
        rs.close();
        stmt.close();   
        return i;
    }
    
    public int getUltimoPapas() throws SQLException {
        Statement stmt = conexion.createStatement();
        int i=0;
        ResultSet rs = stmt.executeQuery(
                        "select *"+ " from InventarioPapas");
      while(rs.next()) {
          i++;  
        }
        rs.close();
        stmt.close();   
        return i;
    }
    
    public int getUltimoAgua() throws SQLException {
        Statement stmt = conexion.createStatement();
        int i=0;
        ResultSet rs = stmt.executeQuery(
                        "select *"+ " from InventarioAgua");
      while(rs.next()) {
          i++;  
        }
        rs.close();
        stmt.close();   
        return i;
    }

    @Override
    public void eliminar(Ingrediente elemento) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
