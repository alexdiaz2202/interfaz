/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexd
 */
public class Menu {
    int numeroDePlatillo[];
    double costo[];
    String nombre[];
    Platillos platillo[] = menuDefinido();

    /**
     * Menu por default que creamos para presentar un ejemplo del programa
     */
    public Menu() {
        platillo = menuDefinido();
        numeroDePlatillo = new int[platillo.length];
        costo = new double[platillo.length];
        nombre = new String[platillo.length];
        for (int i = 0; i < platillo.length; i++) {
            numeroDePlatillo[i]= platillo[i].getNumeroPlatillo();
            costo[i] = platillo[i].getCosto();
            nombre[i] = platillo[i].getNombrePlatillo();
        }
    }

    /**
     * Arreglo de platillos que sirve para nuestra muestra del proyecto
     * @return menu in n out
     */
    public static Platillos[] menuDefinido(){
        String ingredientesDoubleDouble[] = {"Carne", "Carne", "Pan", "Jitomate",
            "Queso", "Queso", "Lechuga", "Cebolla"};
       String ingredientesCheeseBurger[] = {"Carne", "Pan", "Jitomate",
            "Queso", "Lechuga", "Cebolla"};
       String ingredientesBurger[] = {"Carne", "Pan", "Jitomate", "Lechuga", "Cebolla"};
       String ingredientesPapas[] = { "Papas"};
       String ingredientesRefresco[]= {"Agua"};
       String ingredientesCombo1[] = {"Carne", "Carne", "Pan", "Jitomate",
            "Queso", "Queso", "Lechuga", "Cebolla","Papas","Agua"};
       String ingredientesCombo2[] = {"Carne", "Pan", "Jitomate",
            "Queso", "Lechuga", "Cebolla","Papas","Agua"};
       String ingredientesCombo3[] = { "Carne", "Pan", "Jitomate", "Lechuga", 
            "Cebolla","Papas","Agua"};
       
       Platillos DoubleDouble = new Platillos(4, "DoubleDouble", ingredientesDoubleDouble, 69.5);
       Platillos CheeseBurger = new Platillos(5,"Cheeseburger",ingredientesCheeseBurger,55.50);
       Platillos Burger = new Platillos(6,"Burger",ingredientesBurger,45.5);
       Platillos Papas = new Platillos(7,"Papas",ingredientesPapas,19.0);
       Platillos Refresco = new Platillos(8,"Refresco", ingredientesRefresco,15.50);
       Platillos Combo1 = new Platillos(1,"Combo 1",ingredientesCombo1,129.5);
       Platillos Combo2 = new Platillos(2,"Combo 2",ingredientesCombo2,115.0);
       Platillos Combo3 = new Platillos(3,"Combo 3",ingredientesCombo3,99.50);
       
       Platillos[] p ={Combo1,Combo2,Combo3,DoubleDouble,CheeseBurger,Burger,Papas,Refresco};
       return p;
               
    }
    
    /**
     * menu que recibe un arreglo de platillos para poder cambiar el menu por algo distinto
     * y pueda usarse en distintos contextos
     * @param platillos 
     */
    public Menu(Platillos[] platillos){
        platillo = platillos;
        numeroDePlatillo = new int[platillos.length];
        costo = new double[platillos.length];
        nombre = new String[platillos.length];
        for (int i = 0; i < platillos.length; i++) {
            numeroDePlatillo[i]= platillos[i].getNumeroPlatillo();
            costo[i] = platillos[i].getCosto();
            nombre[i] = platillos[i].getNombrePlatillo();
        }
    }


    /**
     * devuelve los ingredientes de un platillo
     * funciona como ayuda para eliminar los ingredientes al momento de realizar una orden
     * @param indice
     * @return 
     */
    public String[] ingredientesPlatillo(int indice){
        return platillo[indice].getIngredientes();
    }
    
    /**
     * Regresa el nombre del platillo
     * @param indice
     * @return 
     */
    public String nombreDePlatillo(int indice){
        return nombre[indice];
    }
    
    /**
     * Regresa el numero del platillo, que sirve como identificador de cada platillo
     * @return 
     */
    public int[] getNumeroDePlatillo() {
        return numeroDePlatillo;
    }

    /**
     * devuelve el costo del platillo
     * @return platillo
     */
    public double[] getCosto() {
        return costo;
    }

    /**
     * Regresa el nombre del platillo
     * @return 
     */
    public String[] getNombre() {
        return nombre;
    }

    /**
     * Regresa el arreglo de platillos que compone el menu
     * @return platillo[]
     */
    public Platillos[] getPlatillo() {
        return platillo;
    }
    
    /**
     * Metodo que regresa un platillo del arreglo de platillos en una posicion especifica;
     * @param indice
     * @return Platillo
     */
    public Platillos getPlatillo(int indice){
        Platillos p = new Platillos();
        for (Platillos platillo1 : platillo) {
            if (platillo1.getNumeroPlatillo() == indice-1) {
                p = platillo1;
            }
        }
        return p;
    }
    
    @Override
    public String toString() {
        String s="No | Platillo | Costo $";
        for (int i = 0; i < platillo.length; i++) {
            s +="\n" + numeroDePlatillo[i]+ " | " +nombre[i] + " | " + costo[i];
        }
        return s;
    }
    

    
    
    
}
