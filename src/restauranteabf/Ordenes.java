
package restauranteabf;
import java.sql.SQLException;
import mx.unam.fciencias.estructuras.lineales.ColaDinamica;


public class Ordenes  {
    
    ColaDinamica cola;  
    Platillos ricos ;//porque son platillos ricos los de nuestro restaurante
    int numOrden ;  
    Menu menu = new Menu();
    InventarioGeneral inventarios;
    
    /**
     * Constructor por omision de las ordenes de una mesa
     */
    public Ordenes(){
        cola = new ColaDinamica();
        numOrden=0;
    }
    
    /**
     * Conttructor por parametros que ya recibe una cola de ordenes y el tamanio de la misma
     * @param cola
     * @param numOrden 
     */
    public Ordenes(ColaDinamica cola, int numOrden) { 
        this.cola = cola;
        this.numOrden = numOrden;
    } 

    /**
     * Devuelve la cantidad de ordenes
     * @return 
     */
    public int getNumOrden() {
        return numOrden;
    }

    /**
     * Puedes cambiar el numero de la orden
     * @param numOrden 
     */
    public void setNumOrden(int numOrden) {
        this.numOrden = numOrden;
    }

    /**
     * agrega platillo a una orden
     * @param ricos
     */
    public void setPlatillo(Platillos ricos) {
        this.cola.insertar(ricos);
    }

    
    /**
     * Metodo que agrega un platillo a la orden de una mesa tomando en cuenta
     * como referencia el menu
     * @param numeroDePlatillo
     * @param inventarios 
     */
    public void agregar(int numeroDePlatillo,InventarioGeneral inventarios) throws SQLException{
        Platillos p[] = menu.getPlatillo();
        cola.insertar(p[numeroDePlatillo-1]);
        agregarOrden(numeroDePlatillo, inventarios);
        numOrden++;
    }

    /**
     * Metodo que elimina los ingredientes utilizados para la elaboracion del platillo
     * @param numeroPlatillo
     * @param inventarios 
     */
    public void agregarOrden(int numeroPlatillo, InventarioGeneral inventarios) throws SQLException{
            IngredientesDAO dao = new IngredientesDAO();
            String[] s = menu.ingredientesPlatillo(numeroPlatillo-1);
            for (int i = 0; i < inventarios.tamanioInventarioGeneral(); i++) {
                    for (int j = 0; j < s.length; j++) {
                        if(inventarios.getTipoInventario(i).equals(s[j])){
                            inventarios.eliminarIngredienteInventario(i);
                            dao.eliminar(inventarios.returnIngrediente(i));
                        }
                    }
                }
    }
    /*public void agregarOrden(int numeroPlatillo, InventarioGeneral inventarios){
        for (int i = 0; i <inventarios.tamanioInventarioGeneral() ; i++) {
            String[] s = menu.ingredientesPlatillo(numeroPlatillo+1);
            for (int j = 0; j < inventarios.tamanioInventarioGeneral(); j++) {
                if(s[i].equals(inventarios.getTipoInventario(j)))
                inventarios.eliminarIngredienteInventario(j);
            }
        }
    }*/
    /**
     * Saca ordenes
     */
    public void ordenFuera(){ 
        cola.remover();    
    } 
    
    
    @Override
    public String toString() {
        return cola.toString();
    }
    
    
}


