/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;


import java.sql.Connection; 
import java.sql.DriverManager;
import java.sql.ResultSet; 
import java.sql.SQLException; 
import java.sql.Statement; 
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ferxd
 */
public class IngredientesDAO implements DAOGenerico<Ingrediente, String> {
    static final String DATABASE_URL="jdbc:sqlite:restaurante.db";
    private final Connection conexion;
    
    //Metodo constructor
    public IngredientesDAO() throws SQLException{
        conexion=DriverManager.getConnection(DATABASE_URL);
    }
    
    //Metodo que inserta in ingrediente a la tabla
    @Override
    public void insertar(Ingrediente ingrediente) throws SQLException {
        Statement stmt=conexion.createStatement();
        stmt.executeUpdate("insert into Ingredientes values ('"+ingrediente.getNombreIngrediente()+"')");
        stmt.close();
    }
    
    //Metodo que elimina a un ingrediente de la tabla
    @Override
    public void eliminar(Ingrediente ingrediente) throws SQLException{
        try (Statement stmt = conexion.createStatement()) {
            stmt.executeUpdate("delete from Ingredientes where nombreIngrediente= '"+ingrediente.getNombreIngrediente()+"'");
        }
    }
    
    //Metodo que nos muestra todos los ingredientes con un nombre especifico
    @Override
    public Ingrediente get(String nombreI) throws SQLException {
        Statement stmt = conexion.createStatement();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from Ingredientes"
                      + " where nombreIngrediente = "
                      + "'" + nombreI + "';"
                );
                
        rs.next();
        Ingrediente ingrediente = new Ingrediente(rs.getString(1) 
                                   );
        
        rs.close();
        stmt.close();
        
        return ingrediente;
                
    }
    
    //Metodo que nos Muestra toda la tabla Ingredientes
    public List<Ingrediente> listar() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<Ingrediente> ingredientes = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from Ingredientes"
                );
        
        while(rs.next()) {
            Ingrediente ingrediente = new Ingrediente(rs.getString(1) 
                                  );
            
            ingredientes.add(ingrediente);
        }
        
        rs.close();
        stmt.close();
        
        return ingredientes;
    }

    //Metodo que actualiza una tabla
    @Override
    public void actualizar(Ingrediente ingrediente) throws SQLException {
        Statement stmt = conexion.createStatement();
        
        stmt.executeUpdate(
                        "update Ingredientes\n" +
                        "set nombreIngrediente = '" + ingrediente.getNombreIngrediente()
                                + "',\n" 
                 
                );
        
        stmt.close();
    }
    
    ////Metodo que cierra la conexion
    public void cerrarConexion() throws SQLException {
        conexion.close();
    }
    
      public static void main(String[] args) throws SQLException {
        IngredientesDAO dao = new IngredientesDAO();
        
//       // System.out.println(dao.get("tomate"));
//        for (Ingrediente ingrediente: dao.listar()) {
//            System.out.println(ingrediente);
//        }
        Ingrediente ingrediente = new Ingrediente("Carne");
//        dao.actualizar(ingrediente);
        dao.insertar(ingrediente);
//        System.out.println(dao.get("lechuga"));
        
        dao.listar().stream().forEach(System.out::println);
        
        dao.cerrarConexion();
    }
    
    
}
