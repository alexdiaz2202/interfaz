
package restauranteabf;

public class TrabajadorRest {
    
    double sueldo; 
    String nombre; 
    int edad;  
    String curp; 

    /**
     * Constructor por omision de un trabajador
     */
    public TrabajadorRest(){
        this.sueldo = 5000; 
        this.edad = 19; 
        this.nombre = "Irving Alexandro Diaz Tapia"; 
        this.curp = "DITIA001012HMCRTRA7"; 
    }
    
    /**
     * Constructor que recibe los datos de un trabajador para contratarlo
     * @param sueldo
     * @param nombre
     * @param edad
     * @param curp 
     */
    public TrabajadorRest(double sueldo, String nombre, int edad, String curp) {
        this.sueldo = sueldo;
        this.nombre = nombre;
        this.edad = edad;
        this.curp = curp;  
        
    } 
    
//    public void agregarTrabajador(TrabajadorRest otro){
//    this.antiguedad.insertar(otro);
//    }

    /**
     * Devuelve el sueldo de un empleado
     * @return 
     */
    public double getSueldo() {
        return sueldo;
    }

    /**
     * Devuelve el nombre de un empleado
     * @return 
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * regresa la edad del trabajador
     * @return 
     */
    public int getEdad() {
        return edad;
     
    }

    /**
     * Devuelve la curp de un trabajador
     * @return 
     */
    public String getCurp() {
        return curp;
    }  

    /**
     * Permite asignar un sueldo al trabajador
     * @param sueldo 
     */
    public void setSueldo(double sueldo) {
        if(sueldo > 120 && sueldo < 12000 )
            
        this.sueldo = sueldo;
    }

    /**
     * Cambia el nombre de un trabajador
     * @param nombre 
     */
    public void setNombre(String nombre) {
        if(nombre.length() < 45)
        this.nombre = nombre;
    }

    /**
     * Metodo que cambia la edad de un tabajador
     * @param edad 
     */
    public void setEdad(int edad) {
        if(edad >= 18 && edad <= 65 )
        this.edad = edad;
    }

    /**
     * metodo que cambia la curp de un trabajador
     * @param curp 
     */
    public void setCurp(String curp) {
        if(curp.length() == 14)
        this.curp = curp;
    }
    
//    public double pagarTrabajador (double dinero ){
//        if(dinero > 120 || dinero < 12000){
//        this.sueldo = dinero;
//        return sueldo;
//             }
//        return 0;
//    }     
     
    
    @Override
    public String toString (){  
        return "Nombre del trabajador: " +getNombre()+ "\n"+ "Curp: " + getCurp()+
              "\n" +"Sueldo: " + getSueldo()   ; 
    }
}
    
    
    
    
    

