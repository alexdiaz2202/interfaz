/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;
import mx.unam.fciencias.estructuras.lineales.ColaDinamica;
/**
 *
 * @author Hp beats
 */
public class Inventario {
    Ingrediente ingrediente;
    int tamanio;//tamaño del inventario
    ColaDinamica colaInventario;
    String inventarioDe; //nombre del tipo de ingrediente que va a guardar el inventario
    
    
    /**
     * Constructor que recibe el nombre del ingrediente de lo que sera el inventario
     * @param nombreInv 
     */
    public Inventario(String nombreInv){
        this.inventarioDe = nombreInv;
        this.colaInventario = new ColaDinamica<>();
        tamanio=0;
    }
    
    /**
     * Metodo que agrega ingredientes a la cola
     * @param ingredinte 
     */
    public void agregarStrock(Ingrediente ingredinte){
        validarIngrediente(ingredinte);
        colaInventario.insertar(ingredinte);
        tamanio++;
    }
    
    /**
     * Metodo que remueve ingredientes de la cola
     */
    public void quitarStock(){
        colaInventario.remover();
        tamanio--;
    }

    /**
     * Metodo que recibe el ingrediente para ver si el inventario es del mismo ingrediente que el que
     * quiere ingresar
     * @param ingredinte 
     */
    public void validarIngrediente(Ingrediente ingredinte){
        if(!inventarioDe.equals(ingredinte.getNombreIngrediente()))
            throw new NoSuchElementException();
    }
    
    /**
     * Regresa el tipo de inventario
     * @return 
     */
    public String getInventarioDe() {
        return inventarioDe;
    }
    
    /**
     *Regresa el tipo de inventario
     * @return 
     */
    public String getIngredienteInventario() {
        return inventarioDe;
    }
    
    /**
     * Regresa la cola de lo que se compone el inventario
     * @return 
     */
    public ColaDinamica getCola() {
        return colaInventario;
    }
    
    @Override
    public String toString() {
        return "En el inventario de "+ inventarioDe + " hay: "+ tamanio + " "
                + inventarioDe +" "
                +"\n" + colaInventario.toString(); 
    }
}

