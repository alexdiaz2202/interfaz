             /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ferxd
 */
public class TrabajadoresDAO implements DAOGenerico<TrabajadorRest, String>{
    static final String DATABASE_URL="jdbc:sqlite:restaurante.db";
    private final Connection conexion;
    
    //Metodo constructor
    public TrabajadoresDAO() throws SQLException{
        conexion=DriverManager.getConnection(DATABASE_URL);
    }
    
    //Metodo que inserta un elemento en la tabla
    @Override
    public void insertar(TrabajadorRest trabajador) throws SQLException {
        Statement stmt=conexion.createStatement();
        stmt.executeUpdate("insert into Trabajadores values ('"+trabajador.getCurp()+"', '"+trabajador.getNombre()+"', '"+trabajador.getEdad()+"', '"+trabajador.getSueldo()+"')");
        stmt.close();
    }
    
    //Metodo que elimina un elemento de la tabla
    @Override
    public void eliminar(TrabajadorRest trabajador) throws SQLException{
        Statement stmt=conexion.createStatement();
        stmt.executeUpdate("delete from Trabajadores where curp= '"+trabajador.getCurp()+"'");
        stmt.close();
    }
    
    //Metodo que nos devuelve un elemento de la tabla
    @Override
    public TrabajadorRest get(String curp) throws SQLException {
        Statement stmt = conexion.createStatement();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from Trabajadores"
                      + " where curp = "
                      + "'" + curp + "';"
                );
                
        rs.next();
        TrabajadorRest trabajador = new TrabajadorRest(rs.getDouble(4), 
                                   rs.getString(2), rs.getInt(3), rs.getString(1));
        
        rs.close();
        stmt.close();
        
        return trabajador;
                
    }
    
    //Metodo que nos muestra toda la tabla
    public List<TrabajadorRest> listar() throws SQLException {
        Statement stmt = conexion.createStatement();
        List<TrabajadorRest> trabajadores = new ArrayList<>();
        
        ResultSet rs = 
                stmt.executeQuery(
                        "select *"
                      + " from Trabajadores"
                );
        
        while(rs.next()) {
            TrabajadorRest trabajador = new TrabajadorRest(rs.getDouble(4), 
                                   rs.getString(2), rs.getInt(3), rs.getString(1));
            
            trabajadores.add(trabajador);
        }
        
        rs.close();
        stmt.close();
        
        return trabajadores;
    }

    //Metodo que actualiza un elemento de la tabla
    @Override
    public void actualizar(TrabajadorRest trabajador) throws SQLException {
        Statement stmt = conexion.createStatement();
        
        stmt.executeUpdate(
                        "update Trabajadores\n" +
                        "set curp = '" + trabajador.getCurp()
                                + "',\n" +
                        "    nombre = '" + trabajador.getNombre() + "'\n" +
                        "    edad = '" + trabajador.getEdad() +
                        "where sueldo = " + trabajador.getSueldo()
                );
        
        stmt.close();
    }
    
    //Metodo que cierra la conexion
    public void cerrarConexion() throws SQLException {
        conexion.close();
    }
    
      public static void main(String[] args) throws SQLException {
        TrabajadoresDAO dao = new TrabajadoresDAO();
        
//       // System.out.println(dao.get("tomate"));
//        for (Ingrediente ingrediente: dao.listar()) {
//            System.out.println(ingrediente);
//        }
        
//            TrabajadorRest gerentexd = new TrabajadorRest(3500.00, "Fernando", 19, "thebest1234567890");
            TrabajadorRest gerente2 = new TrabajadorRest(3500.00, "Brian", 19, "thebest2345678901");
            TrabajadorRest gerente3 = new TrabajadorRest(3500.00, "Alex", 19, "thebest3456789012");
//        dao.insertar(gerentexd);
        dao.insertar(gerente2);
        dao.insertar(gerente3);
//        System.out.println(dao.get("lechuga"));
//          dao.eliminar(nuevoTrabajador1);
//          dao.eliminar(nuevoTrabajador2);
        
        dao.listar().stream().forEach(System.out::println);
        
        dao.cerrarConexion();
    }
    
}
