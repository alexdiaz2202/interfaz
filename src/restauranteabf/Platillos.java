/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;

import java.util.Arrays;

/**
 *
 * @author Hp beats
 */
public class Platillos { 
    int numeroPlatillo;
    String nombrePlatillo; 
    String ingredientes[] ; 
    String extras; 
    double costo;  

    /**
     * Constructor por omision de un platillo
     */
    public Platillos() {
        numeroPlatillo = 0;
        nombrePlatillo="";
        extras = "";
        costo = 0;
    }

    /**
     * Constructor que recibe parametros para la creacion de un platillo
     * @param numeroPlatillo
     * @param nombrePlatillo
     * @param ingredientes
     * @param costo 
     */
    public Platillos(int numeroPlatillo, String nombrePlatillo, String[] ingredientes, double costo) {
        this.numeroPlatillo = numeroPlatillo;
        this.nombrePlatillo = nombrePlatillo;
        this.ingredientes = ingredientes;
        this.costo = costo;
    }
    
    /**
     * Metodo que devuelve el nombre del paltillo
     * @return 
     */
    public String getNombrePlatillo() {
        return nombrePlatillo;
    }

    /**
     * Metodo que devuelve un arreglo con los ingredientes de cada platillo
     * @return 
     */
    public String[] getIngredientes() {
        return ingredientes;
    }

    /**
     * Metodo para cambiar el arreglo de ingredientes de un platillo
     * @param ingredientes 
     */
    public void setIngredientes(String[] ingredientes) {
        this.ingredientes = ingredientes;
    }
    
    /**
     * devuelve costo del platillo
     * @return 
     */
    public double getCosto() {
        return costo;
    }

    //Metodo con el cual le podemos dar un costo a un platillos
    public void setCosto(double costo) {
        this.costo = costo;
    }
    
    //Nos regresa el costo del platillos
    public double getPlatilloCosto(){
         return costo;
    }

    @Override
    public String toString() {
        return "Usted pidió: " + getNombrePlatillo() +
                " Con: " + Arrays.toString(getIngredientes())+" $ " + getCosto();
    }

    /**
     * Regresa el numero que identifica al platillo
     * @return 
     */
    public int getNumeroPlatillo() {
        return numeroPlatillo;
    }
    
    
    
 
   
}
