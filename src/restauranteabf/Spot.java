/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;

import java.sql.SQLException;

/**
 *
 * @author Hp beats
 */
public class Spot {
    int nMesa; //numero de mesa
    boolean mesaDisponible;
    double cuentaDeLaMesa;
    Ordenes ord;
    Menu menu = new Menu();
    
    /**
     * Constructor que recibe el numero de la mesa para
     * identificar a esta de las otras
     * @param nMesa 
     */
    public Spot(int nMesa) {
        this.nMesa = nMesa;
        cuentaDeLaMesa = 0;
        ord = new Ordenes();
        mesaDisponible = true;
    }
    
    /**
     * Metodo que devuelve el numero de la mesa
     * @return 
     */
    public int getnMesa() {
        return nMesa;
    }
    
    /**
     * Metodo que devuelve la cuenta que adeuda la mesa
     * @return 
     */
    public double getCuentaDeLaMesa() {
        return cuentaDeLaMesa;
    }

    /**
     * metodo que dice si una mesa esta disponible o no
     * @return 
     */
    public boolean isMesaDisponible() {
        return mesaDisponible;
    }
    
    /**
     * Metodo que cambia el numero de la mesa
     * @param nMesas 
     */
    public void setnMesas(int nMesas) {
        this.nMesa = nMesas;
    }

    /**
     * metodo que puede cambiar la cuenta de la mesa
     * antes de pagarla
     * @param cuentaDeLaMesa 
     */
    public void setCuentaDeLaMesa(double cuentaDeLaMesa) {
        this.cuentaDeLaMesa = cuentaDeLaMesa;
    }

    /**
     * Podemos cambiar la dispoibilidad de la mesa
     * @param mesaDisponible 
     */
    public void setMesaDisponible(boolean mesaDisponible) {
        this.mesaDisponible = mesaDisponible;
    }

    
    @Override
    public String toString() {
        return "Disponibilidad: " + isMesaDisponible() 
                + "\nLa mesa debe: " + cuentaDeLaMesa +
                "\nOrdeno " + ord.getNumOrden() + " platillos ";
    }
    
    /**
     * metodo que agrega una orden a la mesa y a su vez descuenta los ingredientes 
     * utilizados en el inventario general
     * @param numeroPlatillo
     * @param almacen 
     */
    public void agregarCuenta(int numeroPlatillo, InventarioGeneral almacen) throws SQLException{
        setMesaDisponible(false);
        cuentaDeLaMesa += menu.getPlatillo(numeroPlatillo).getPlatilloCosto();
        ord.agregar(numeroPlatillo,almacen);
    }
    
    /**
     * Cancela la cuenta de una mesa
     */
    public void cancelarCuenta(){
        cuentaDeLaMesa = 0;
    }
    
    /**
     * Metodo para pagar la cuenta de una mesa y que esta se vea reflejada 
     * en las ventas del dia en la caja
     * @param caja 
     */
    public void pagarCuenta(CajaRest caja){
        setMesaDisponible(true);
        caja.vender(cuentaDeLaMesa);
        cancelarCuenta();
        ord = new Ordenes();
    }
    
    
}
