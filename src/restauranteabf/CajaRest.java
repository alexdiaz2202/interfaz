package restauranteabf;
import java.util.Date;
public class CajaRest {
  
    double ventasDia; 
    double comprasDia; 
    double balance = ventasDia - comprasDia; 
    double venta; 
    double compra; 
    TrabajadorRest personita = new TrabajadorRest(); 
    Date fecha;
    
    
    /**
     * Constructor por omision con parametros en 0;
     */
    public CajaRest(){
    ventasDia = 0; 
    comprasDia=0;
    venta = 0 ;
    compra = 0;
    fecha = new Date();
    }
    
    /**
     * Constructor que recibe compras y ventas del dia 
     * @param ventasDia
     * @param comprasDia 
     */
    public CajaRest(double ventasDia, double comprasDia ) {
        this.ventasDia = ventasDia;
        this.comprasDia = comprasDia;
        this.venta = 0; 
        this.compra = 0;
        this.balance = ventasDia - comprasDia; 
        fecha = new Date();
    }   
    
    /**
     * Metodo que permite mantener el pago de los trabajadores fijo
     * @param dinero 
     */
    public void pagarTrabajador(double dinero){ 
      if(dinero > 120 && dinero < 12000) 
          personita.sueldo = dinero;
    }
    
    /**
     * Metodo que obtiene las ventas del dia
     * @return VentasDia
     */
    public double getVentasDia() {
        return ventasDia;
    } 

    /**
     * Metodo que obtiene las compras del dia
     * @return comprasDia
     */
    public double getComprasDia() {
        return comprasDia;
    } 

    /**
     * Metodo que devuelve el balance de la caja
     * @return balance
     */
    public double getBalance() {
        return balance;
    } 

    /**
     * Devuelve la fecha en la que se inializo la caja
     * @return fecha
     */
    public Date getFecha() {
        return fecha;
    }
    
    /**
     * Metodo para darle ingresos especiales que tuvo la caja en el dia
     * @param cantidad 
     */
    public void vender(double cantidad){
        this.ventasDia = ventasDia + cantidad;
        this.balance = ventasDia - comprasDia;    
        
    } 
    
    /**
     * Metodo que recibe las compras del dia a surtidres, distribuidores, sueldos, etc...
     * @param cantidad2 
     */
    public void comprar (double cantidad2){
        this.comprasDia = this.comprasDia + cantidad2;
        this.balance = ventasDia - comprasDia; 
    } 
    
    
    
    @Override
    public String toString() {
        
     return "Ventas del dia: " + getVentasDia()+
             "\n" + "Compras del dia: " + getComprasDia()+ 
             "\n" + "Balance: " + getBalance() ; 
        
    }
}
