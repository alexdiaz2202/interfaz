/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restauranteabf;

/**
 *
 * @author alexd
 */
public class InventarioGeneral {
    Inventario[] inventario;

    /**
     * Constructor que recibe un arreglo de inventarios para
     * expresar el almacen o inventario general de todos los inventarios
     * @param inv 
     */
    public InventarioGeneral(Inventario[] inv){
        inventario = inv;
    }
    
    /**
     * Obtiene el tamanio del arreglo de inventarios
     * @return 
     */
    public int tamanioInventarioGeneral(){
        return inventario.length;
    }
    
    /**
     * Devuelve el arreglo de inventarios de lo que se compone
     * @return 
     */
    public Inventario[] getInv() {
        return inventario;
    }
/**
 * devuelve el tipo de inventario en una poisicion especifica
 * @param indice
 * @return 
 */
    public String getTipoInventario(int indice){
        return inventario[indice].getIngredienteInventario();
    }
    
    public void setInv(Inventario[] inv) {
        this.inventario = inv;
    }
   
   public void agregarInventario(Inventario inventario){
       this.inventario[this.inventario.length] = inventario;
   } 
   
   /**
    * Elimina un igrediente de la cola de un inventario en especifico
    * @param indice 
    */
   public void eliminarIngredienteInventario(int indice){
       inventario[indice].quitarStock();
   }
   
   /**
    * metodo que sirve para comprar inventario de inventarios ya existentes
    * asi no necesitas agregar de uno en uno en el inventaro
    * @param nombreIngrediente
    * @param fechaDeCaducidad
    * @param cantidad
    * @param costo
    * @param caja 
    */
   public void comprarInventarioExistente(String nombreIngrediente, int cantidad,
   double costo, CajaRest caja){
       Ingrediente ingredienteComprado = new Ingrediente(nombreIngrediente);
       int i;
       for (i=0;i<inventario.length;i++){
           if(inventario[i].getInventarioDe().equals(nombreIngrediente))
               for (int j = 0; j <= cantidad; j++) {
                   inventario[i].agregarStrock(ingredienteComprado);
                   caja.comprar(costo);
               }
       }
   }

   /**
    * metodo que crea nuevos inventarios en el arreglo de inventarios
    * @param nombre 
    */
  public void crearInventarioNuevo(String nombre){
        Inventario[] inv = new Inventario[inventario.length+1];
        Inventario inventarioNuevo = new Inventario(nombre);
        for (int i = 0; i < inventario.length; i++) {
            inv[i] = inventario[i];
        }
        inventarioNuevo = inv[inventario.length];
        inventario = inv;
    }
   
   public Ingrediente returnIngrediente(int indice){
       return inventario[indice].ingrediente;
   }
   @Override
   public String toString() {
        String s="";
        for (int i = 0; i < inventario.length; i++) {
            s += "\n" + inventario[i].toString();
        }
        return s;
   }
   
   
}
