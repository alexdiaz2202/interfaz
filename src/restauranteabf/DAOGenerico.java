package restauranteabf;

import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ferxd
 */
public interface DAOGenerico<E, K>{
     void insertar(E elemento) throws SQLException;
    E get(K llave) throws SQLException;
    void actualizar(E elemento) throws SQLException;
    void eliminar(E elemento) throws SQLException;
}
