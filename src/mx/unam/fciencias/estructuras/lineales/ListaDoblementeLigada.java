 package mx.unam.fciencias.estructuras.lineales;

import mx.unam.fciencias.estructuras.Nodo;

public class ListaDoblementeLigada<E> 
        implements Lista<E>
{
    private Nodo<E> inicio;
    private Nodo<E> fin;
    private int tamanio;

    private void validarIndice(int indice) {
        if(isVacia() 
            || (indice < 0 
               || indice >= tamanio))
            throw new IndexOutOfBoundsException();
    }
    
    @Override
    public void agregar(E elemento) {
        Nodo<E> nuevo = new Nodo<>(elemento);
        
        if (isVacia()) {
            inicio = fin = nuevo;
        }
        else {
            fin.setDerecho(nuevo);
            nuevo.setIzquierdo(fin);
            fin = nuevo;
        }
        
        tamanio++;
    }

    @Override
    public void agregar(int indice, E elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public E eliminar(int indice) {
        validarIndice(indice);
        
        E elemento = null;
        if(tamanio == 1) {
            elemento = inicio.getElemento();
            inicio = fin = null;
        } else if(indice == 0) {
            elemento = inicio.getElemento();
            inicio = inicio.getDerecho();
            inicio.setIzquierdo(null);
        } else if(indice == tamanio-1) {
            elemento = fin.getElemento();
            fin = fin.getIzquierdo();
            fin.setDerecho(null);
        } else {
            Nodo<E> auxiliar = inicio;
            for (int i = 0; i < indice; i++) {
                auxiliar = auxiliar.getDerecho();
            }
            
            elemento = auxiliar.getElemento();
            auxiliar.getDerecho()
                .setIzquierdo(
                    auxiliar
                        .getIzquierdo());
            auxiliar.getIzquierdo()
                .setDerecho(
                    auxiliar
                        .getDerecho());
        }
        
        tamanio--;
        return elemento;
    }

    @Override
    public void eliminar(E elemento) {
        int indice = indiceDe(elemento);
        
        if (indice != -1) {
            eliminar(indice);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public int indiceDe(E elemento) {
        int indice = 0;
        
        for (Nodo<E> auxiliar = inicio; 
            auxiliar != null; 
            auxiliar = auxiliar.getDerecho(),
                indice++) {
            if(auxiliar.getElemento()
                    .equals(elemento))
                return indice;
        }
        
        return -1;
    }

    @Override
    public E get(int indice) {
        validarIndice(indice);
        
        Nodo<E> auxiliar = inicio;
        for (int i = 0; i < indice; i++) {
            auxiliar = auxiliar.getDerecho();
        }
        
        return auxiliar.getElemento();
    }

    @Override
    public boolean isVacia() {
        return inicio == null;
    }

    @Override
    public int tamanio() {
        return tamanio;
    }

    @Override
    public String toString() {
        String representacion = "";
        
        for (Nodo<E> auxiliar = inicio; 
                auxiliar != null; 
                auxiliar = auxiliar.getDerecho()) {
            representacion += auxiliar + " -> ";
        }
        
        return representacion;
    }
    
}
