/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.unam.fciencias.estructuras.lineales;

/**
 *
 * @author windows
 */
public class NoSuchElementException extends RuntimeException {

    public NoSuchElementException() {
        super("Elemento no encontrado");
    }
    
}
