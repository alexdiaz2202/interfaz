package mx.unam.fciencias.estructuras.lineales;

public class ColaDinamica<E> implements Cola<E> {
    private Lista<E> elementos;

    public ColaDinamica() {
        this.elementos = new ListaDoblementeLigada<>();
    }
    
    @Override
    public void insertar(E elemento) {
        elementos.agregar(elemento);
    }

    @Override
    public E remover() {
        return elementos.eliminar(0);
    }

    @Override
    public String toString() {
        return elementos.toString();
    }
    
    public int getTamanio(){
        return elementos.tamanio();
    }
    
    public E get(int indice){
    return elementos.get(indice);
    
    
    }
   
}
